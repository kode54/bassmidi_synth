CFLAGS = -c -I bass -L bass
LFLAGS = -L bass/x64 -lbassmidi -lbass -lasound

OBJS = main.o


OPTS = -O3

all: bassmidi_synth

bassmidi_synth : $(OBJS)
	$(CC) -o $@ $^ $(LFLAGS)

.c.o:
	$(CC) $(CFLAGS) $(OPTS) -o $@ $*.c

clean:
	rm -f $(OBJS) bassmidi_synth > /dev/null

