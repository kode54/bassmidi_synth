#define _GNU_SOURCE
#include <bass.h>
#include <bassmidi.h>

#include <stdio.h>
#include <signal.h>
#include <time.h>
#include <sched.h>
#include <math.h>
#include <linux/limits.h>

#include <alsa/asoundlib.h>

/* options */
static int opt_sample_rate = 44100;
static int opt_realtime_priority = 10;
static int opt_sequencer_ports = 4;
static int opt_daemonize = 1;

static char * opt_soundfont = NULL;

#define MAX_PORTS	16

#define TICKTIME_HZ	100

static unsigned int font_count[MAX_PORTS] = { 0 };
static HSOUNDFONT * hFonts[MAX_PORTS] = { NULL };
static HSTREAM hStream[MAX_PORTS] = { 0 };

static void FreeFonts(unsigned int uDeviceID);
static void LoadFonts(unsigned int uDeviceID, const char * name);

struct seq_context {
    snd_seq_t *handle;	/* The snd_seq handle to /dev/snd/seq */
    int client;		/* The client associated with this context */
    int num_ports;		/* number of ports */
    int port[MAX_PORTS];	/* created sequencer ports */
    int fd;			/* The file descriptor */
    int used;		/* number of current connection */
    int active;		/* */
    int queue;
    snd_seq_queue_status_t *q_status;
};

typedef struct {
    long time;
    unsigned char channel;
    unsigned char type;
    unsigned char a, b;
    long data_length;
    unsigned char * data;
} MidiEvent;

#define ME_NOTEOFF           8
#define ME_NOTEON            9
#define ME_KEYPRESSURE      10
#define ME_CONTROL          11
#define ME_PROGRAM          12
#define ME_CHANNEL_PRESSURE 13
#define ME_PITCHWHEEL       14
#define ME_SYSTEM_EXCLUSIVE 15

static struct seq_context alsactx;

static int buffer_time_advance;
static long buffer_time_offset;
static long start_time_base;
static long cur_time_offset;
static long last_queue_offset;
static double rate_frac, rate_frac_nsec;

static int is_running;

/* signal handlers */
static void sig_timeout(int sig);
static void sig_reset(int sig);
static void sig_close(int sig);

static int set_realtime_priority(void);

#if SND_LIB_MAJOR > 0 || SND_LIB_MINOR >= 6
static int snd_seq_file_descriptor(snd_seq_t *handle);
#endif
static int alsa_seq_open(snd_seq_t **seqp);
static int alsa_create_port(snd_seq_t *seq, int index);
static void alsa_set_timestamping(struct seq_context *ctxp, int port);

static void doit(struct seq_context *ctxp);
static int do_sequencer(struct seq_context *ctxp);
static int start_sequencer(struct seq_context *ctxp);
static void stop_sequencer(struct seq_context *ctxp);
static void server_reset(void);

int main(int argc, char ** argv)
{
    int i;
    BASS_INFO info;

#ifdef SIGPIPE
    signal(SIGPIPE, SIG_IGN);    /* Handle broken pipe */
#endif /* SIGPIPE */
    if (argc != 2) {
        fputs("Usage:\tbassmidi_synth <soundfont.sf2|list.sflist>\n", stderr);
        return 1;
    }
    
    opt_soundfont = argv[1];

    set_realtime_priority();

    if (alsa_seq_open(&alsactx.handle) < 0) {
        fputs("Unable to open ALSA sequencer\n", stderr);
        return 1;
    }
    alsactx.queue = -1;
    alsactx.client = snd_seq_client_id(alsactx.handle);
    alsactx.fd = snd_seq_file_descriptor(alsactx.handle);
    snd_seq_set_client_name(alsactx.handle, "BASSMIDI");
    snd_seq_set_client_pool_input(alsactx.handle, 1000); /* enough? */
    if (opt_sequencer_ports < 1)
        alsactx.num_ports = 1;
    else if (opt_sequencer_ports > MAX_PORTS)
        alsactx.num_ports = MAX_PORTS;
    else
        alsactx.num_ports = opt_sequencer_ports;

    for (i = 0; i < alsactx.num_ports; i++) {
        int port;
        port = alsa_create_port(alsactx.handle, i);
        if (port < 0) {
            fputs("Unable to create ALSA port\n", stderr);
            return 1;
        }
        alsactx.port[i] = port;
        alsa_set_timestamping(&alsactx, port);
    }

    alsactx.used = 0;
    alsactx.active = 0;

    buffer_time_advance = 0;

    rate_frac = (double)opt_sample_rate / 1000000.0;
    rate_frac_nsec = (double)opt_sample_rate / 1000000000.0;

    is_running = 1;

    alarm(0);
    signal(SIGALRM, sig_timeout);
    signal(SIGINT, sig_close);
    signal(SIGTERM, sig_close);
    signal(SIGHUP, sig_reset);

    if (opt_daemonize)
    {
        int pid = fork();
        FILE *pidf;
        switch (pid)
        {
            case 0:			// child is the daemon
                break;
            case -1:		// error status return
                return 7;
            default:		// no error, doing well
                if ((pidf = fopen( "/var/run/bassmidi_synth.pid", "w" )) != NULL )
                    fprintf( pidf, "%d\n", pid );
                return 0;
        }
    }

    BASS_SetConfig(BASS_CONFIG_UPDATEPERIOD, 10);

    BASS_PluginLoad("/usr/local/lib/libbassflac.so", 0);
    BASS_PluginLoad("/usr/local/lib/libbasswv.so", 0);

    if ( !BASS_Init( -1, opt_sample_rate, BASS_DEVICE_LATENCY, NULL, NULL ) ) {
        fprintf(stderr, "Unable to open BASS, error: %d\n", BASS_ErrorGetCode());
        return 1;
    }

    BASS_GetInfo(&info);
    BASS_SetConfig(BASS_CONFIG_BUFFER, 50 + info.minbuf);


    for (i = 0; i < alsactx.num_ports; i++) {
        hStream[i] = BASS_MIDI_StreamCreate( 16, BASS_SAMPLE_FLOAT | BASS_MIDI_SINCINTER, opt_sample_rate );
        if (!hStream[i]) {
            fprintf(stderr, "Unable to create BASS MIDI stream, error code: %d\n", BASS_ErrorGetCode());
            return 1;
        }
        BASS_ChannelPlay( hStream[i], FALSE );
        BASS_MIDI_StreamEvent( hStream[i], 0, MIDI_EVENT_SYSTEM, MIDI_SYSTEM_DEFAULT );
    }

    while (is_running) {
        const struct timespec ts = { 0, 10000000 };
        server_reset();
        doit(&alsactx);
        nanosleep( &ts, NULL );
    }

    for (i = 0; i < alsactx.num_ports; i++) {
        BASS_StreamFree( hStream[i] );
        FreeFonts(i);
    }

    BASS_Free();

    unlink( "/var/run/bassmidi_synth.pid" );

    return 0;
}

static void play_event(const MidiEvent *ev)
{
    unsigned int port;
    unsigned char data[3];
    port = ( ev->channel >> 4 ) & 15;
    switch (ev->type)
    {
    case ME_NOTEOFF:
    case ME_NOTEON:
    case ME_KEYPRESSURE:
    case ME_CONTROL:
    case ME_PITCHWHEEL:
        data[0] = ( ev->channel & 15 ) + ( ev->type << 4 );
        data[1] = ev->a;
        data[2] = ev->b;
        BASS_MIDI_StreamEvents( hStream[port], BASS_MIDI_EVENTS_RAW, data, 3 );
        break;

    case ME_PROGRAM:
    case ME_CHANNEL_PRESSURE:
        data[0] = ( ev->channel & 15 ) + ( ev->type << 4 );
        data[1] = ev->a;
        BASS_MIDI_StreamEvents( hStream[port], BASS_MIDI_EVENTS_RAW, data, 2 );
        break;

    case ME_SYSTEM_EXCLUSIVE:
        BASS_MIDI_StreamEvents( hStream[port], BASS_MIDI_EVENTS_RAW, ev->data, ev->data_length );
        break;
    }
}

void FreeFonts(unsigned int uDeviceID)
{
    unsigned i;
    if ( hFonts[ uDeviceID ] && font_count[ uDeviceID ] )
    {
        BASS_MIDI_StreamSetFonts( hStream[ uDeviceID ], NULL, 0 );
        for ( i = 0; i < font_count[ uDeviceID ]; ++i )
        {
            BASS_MIDI_FontFree( hFonts[ uDeviceID ][ i ] );
        }
        free( hFonts[ uDeviceID ] );
        hFonts[ uDeviceID ] = NULL;
        font_count[ uDeviceID ] = 0;
    }
}

void LoadFonts(unsigned int uDeviceID, const char * name)
{
    FreeFonts(uDeviceID);

    if (name && *name)
    {
        const char * ext = strrchr( name, '.' );
        if ( ext ) ext++;
        if ( !strcasecmp( ext, "sf2" ) || !strcasecmp( ext, "sf2pack" ) )
        {
            font_count[ uDeviceID ] = 1;
            hFonts[ uDeviceID ] = (HSOUNDFONT*)malloc( sizeof(HSOUNDFONT) );
            *hFonts[ uDeviceID ] = BASS_MIDI_FontInit( name, 0 );
        }
        else if ( !strcasecmp( ext, "sflist" ) )
        {
            FILE * fl = fopen( name, "r" );
            font_count[ uDeviceID ] = 0;
            if ( fl )
            {
                char path[PATH_MAX], fontname[PATH_MAX], temp[PATH_MAX];
                const char * filename = strrchr( name, '/' ) + 1;
                if ( filename == (char*)1 ) filename = name;
                strncpy( path, name, filename - name );
                path[ filename - name ] = 0;
                while ( !feof( fl ) )
                {
                    char * cr, * slash;
                    if( !fgets( fontname, PATH_MAX, fl ) ) break;
                    fontname[PATH_MAX-1] = 0;
                    cr = strrchr( fontname, '\n' );
                    if ( cr ) *cr = 0;
                    cr = strrchr( fontname, '\r' );
                    if ( cr ) *cr = 0;
                    if ( fontname[0] == '/' )
                    {
                        cr = fontname;
                    }
                    else
                    {
                        size_t i = strlen(path) + strlen(fontname);
                        strncpy( temp, path, PATH_MAX );
                        strncat( temp, fontname, i < PATH_MAX ? i : PATH_MAX );
                        temp[PATH_MAX - 1] = 0;
                        cr = temp;
                    }
                    while (slash = strchr(cr, '\\')) *slash = '/';
                    font_count[ uDeviceID ]++;
                    hFonts[ uDeviceID ] = (HSOUNDFONT*)realloc( hFonts[ uDeviceID ], sizeof(HSOUNDFONT) * font_count[ uDeviceID ] );
                    hFonts[ uDeviceID ][ font_count[ uDeviceID ] - 1 ] = BASS_MIDI_FontInit( cr, 0 );
                }
                fclose( fl );
            }
        }
        if (font_count[ uDeviceID ]) {
            int i;
            BASS_MIDI_FONT * mf = (BASS_MIDI_FONT*)malloc( sizeof(BASS_MIDI_FONT) * font_count[ uDeviceID ] );
            for ( i = 0; i < font_count[ uDeviceID ]; ++i ) {
                mf[i].font = hFonts[ uDeviceID ][ font_count[ uDeviceID ] - i - 1 ];
                mf[i].preset = -1;
                mf[i].bank = 0;
            }
            BASS_MIDI_StreamSetFonts( hStream[ uDeviceID ], mf, font_count[ uDeviceID ] );
            free(mf);
        }
    }
}

/*
 * get the current time in usec from gettimeofday()
 */
static long get_current_time(void)
{
    struct timeval tv;
    long t;

    gettimeofday(&tv, NULL);
    t = tv.tv_sec * 1000000L + tv.tv_usec;
    return t - start_time_base;
}

/*
 * convert from snd_seq_real_time_t to sample count
 */
inline static long queue_time_to_position(const snd_seq_real_time_t *t)
{
    return (long)t->tv_sec * opt_sample_rate + (long)(t->tv_nsec * rate_frac_nsec);
}

/*
 * get the current queue position in sample count
 */
static long get_current_queue_position(struct seq_context *ctxp)
{
#if HAVE_SND_SEQ_PORT_INFO_SET_TIMESTAMPING
    snd_seq_get_queue_status(ctxp->handle, ctxp->queue, ctxp->q_status);
    return queue_time_to_position(snd_seq_queue_status_get_real_time(ctxp->q_status));
#else
    return 0;
#endif
}

/*
 * update the current position from the event timestamp
 */
static void update_timestamp_from_event(snd_seq_event_t *ev)
{
    long t = queue_time_to_position(&ev->time.time) - last_queue_offset;
    if (t < 0) {
        // fprintf(stderr, "timestamp underflow! (delta=%d)\n", (int)t);
        t = 0;
    } else if (buffer_time_advance > 0 && t >= buffer_time_advance) {
        // fprintf(stderr, "timestamp overflow! (delta=%d)\n", (int)t);
        t = buffer_time_advance - 1;
    }
    t += buffer_time_offset;
    if (t >= cur_time_offset)
        cur_time_offset = t;
}

/*
 * update the current position from system time
 */
static void update_timestamp(void)
{
    cur_time_offset = (long)(get_current_time() * rate_frac);
}

static void seq_play_event(MidiEvent *ev)
{
    ev->time = cur_time_offset;
    play_event(ev);
}

static void stop_playing(void)
{
}

static void doit(struct seq_context *ctxp)
{
    for (;;) {
        while (snd_seq_event_input_pending(ctxp->handle, 1)) {
            if (do_sequencer(ctxp))
                goto __done;
        }
        if (ctxp->active) {
            /* update the current position */
            if (ctxp->queue >= 0)
                cur_time_offset = get_current_queue_position(ctxp);
            else
                update_timestamp();
        }
        {
            fd_set rfds;
            struct timeval timeout;
            FD_ZERO(&rfds);
            FD_SET(ctxp->fd, &rfds);
            timeout.tv_sec = 0;
            timeout.tv_usec = 10000; /* 10ms */
            if (select(ctxp->fd + 1, &rfds, NULL, NULL, &timeout) < 0)
                goto __done;
        }
    }

__done:
    if (ctxp->active) {
        stop_sequencer(ctxp);
    }
}

static void server_reset(void)
{
    int i;
    for (i = 0; i < alsactx.num_ports; i++) {
        FreeFonts(i);
    }
}

static int start_sequencer(struct seq_context *ctxp)
{
    int i;

    for (i = 0; i < alsactx.num_ports; i++) {
        LoadFonts(i, opt_soundfont);
    }

    ctxp->active = 1;

    buffer_time_offset = 0;
    last_queue_offset = 0;
    cur_time_offset = 0;
    if (ctxp->queue >= 0) {
        if (snd_seq_start_queue(ctxp->handle, ctxp->queue, NULL) < 0)
            ctxp->queue = -1;
        else
            snd_seq_drain_output(ctxp->handle);
    }
    if (ctxp->queue < 0) {
        start_time_base = 0;
        start_time_base = get_current_time();
    }

    return 1;
}

static void stop_sequencer(struct seq_context *ctxp)
{
    int i;
    stop_playing();
    if (ctxp->queue >= 0) {
        snd_seq_stop_queue(ctxp->handle, ctxp->queue, NULL);
        snd_seq_drain_output(ctxp->handle);
    }
    for (i = 0; i < alsactx.num_ports; i++) {
        FreeFonts(i);
    }
    ctxp->used = 0;
    ctxp->active = 0;
}

#define NOTE_CHAN(ev)	((ev)->dest.port * 16 + (ev)->data.note.channel)
#define CTRL_CHAN(ev)	((ev)->dest.port * 16 + (ev)->data.control.channel)

static int do_sequencer(struct seq_context *ctxp)
{
    int n, ne, i;
    MidiEvent ev;
    snd_seq_event_t *aevp;

    n = snd_seq_event_input(ctxp->handle, &aevp);
    if (n < 0 || aevp == NULL)
        return 0;

    if (ctxp->active && ctxp->queue >= 0)
        update_timestamp_from_event(aevp);
    else
        update_timestamp();

    switch(aevp->type) {
    case SND_SEQ_EVENT_NOTEON:
        ev.channel = NOTE_CHAN(aevp);
        ev.a       = aevp->data.note.note;
        ev.b       = aevp->data.note.velocity;
        if (ev.b == 0)
            ev.type = ME_NOTEOFF;
        else
            ev.type = ME_NOTEON;
        seq_play_event(&ev);
        break;

    case SND_SEQ_EVENT_NOTEOFF:
        ev.channel = NOTE_CHAN(aevp);
        ev.a       = aevp->data.note.note;
        ev.b       = aevp->data.note.velocity;
        ev.type = ME_NOTEOFF;
        seq_play_event(&ev);
        break;

    case SND_SEQ_EVENT_KEYPRESS:
        ev.channel = NOTE_CHAN(aevp);
        ev.a       = aevp->data.note.note;
        ev.b       = aevp->data.note.velocity;
        ev.type = ME_KEYPRESSURE;
        seq_play_event(&ev);
        break;

    case SND_SEQ_EVENT_PGMCHANGE:
        ev.channel = CTRL_CHAN(aevp);
        ev.a = aevp->data.control.value;
        ev.type = ME_PROGRAM;
        seq_play_event(&ev);
        break;

    case SND_SEQ_EVENT_CONTROLLER:
        ev.type = ME_CONTROL;
        ev.channel = CTRL_CHAN(aevp);
        ev.a = aevp->data.control.param;
        ev.b = aevp->data.control.value;
        seq_play_event(&ev);
        break;

    case SND_SEQ_EVENT_CONTROL14:
        if (aevp->data.control.param < 0 || aevp->data.control.param >= 32)
            break;
        ev.type = ME_CONTROL;
        ev.channel = CTRL_CHAN(aevp);
        ev.a = aevp->data.control.param;
        ev.b = (aevp->data.control.value >> 7) & 0x7f;
        seq_play_event(&ev);
        ev.a += 32;
        ev.b = aevp->data.control.value & 0x7f;
        seq_play_event(&ev);
        break;

    case SND_SEQ_EVENT_PITCHBEND:
        ev.type    = ME_PITCHWHEEL;
        ev.channel = CTRL_CHAN(aevp);
        aevp->data.control.value += 0x2000;
        ev.a       = (aevp->data.control.value) & 0x7f;
        ev.b       = (aevp->data.control.value>>7) & 0x7f;
        seq_play_event(&ev);
        break;

    case SND_SEQ_EVENT_CHANPRESS:
        ev.type    = ME_CHANNEL_PRESSURE;
        ev.channel = CTRL_CHAN(aevp);
        ev.a       = aevp->data.control.value;
        seq_play_event(&ev);
        break;

    case SND_SEQ_EVENT_NONREGPARAM:
        /* Break it back into its controler values */
        ev.type = ME_CONTROL;
        ev.channel = CTRL_CHAN(aevp);
        ev.a = 99;
        ev.b = (aevp->data.control.param >> 7) & 0x7f;
        seq_play_event(&ev);
        ev.a = 98;
        ev.b = aevp->data.control.param & 0x7f;
        seq_play_event(&ev);
        ev.a = 6;
        ev.b = (aevp->data.control.value >> 7) & 0x7f;
        seq_play_event(&ev);
        ev.a = 38;
        ev.b = aevp->data.control.value & 0x7f;
        seq_play_event(&ev);
        break;

    case SND_SEQ_EVENT_REGPARAM:
        /* Break it back into its controler values */
        ev.type = ME_CONTROL;
        ev.channel = CTRL_CHAN(aevp);
        ev.a = 101;
        ev.b = (aevp->data.control.param >> 7) & 0x7f;
        seq_play_event(&ev);
        ev.a = 100;
        ev.b = aevp->data.control.param & 0x7f;
        seq_play_event(&ev);
        ev.a = 6;
        ev.b = (aevp->data.control.value >> 7) & 0x7f;
        seq_play_event(&ev);
        ev.a = 38;
        ev.b = aevp->data.control.value & 0x7f;
        seq_play_event(&ev);
        break;

    case SND_SEQ_EVENT_SYSEX:
        ev.type = ME_SYSTEM_EXCLUSIVE;
        ev.channel = aevp->dest.port * 16;
        ev.data_length = aevp->data.ext.len;
        ev.data = aevp->data.ext.ptr;
        seq_play_event(&ev);
        break;

#if SND_LIB_MAJOR > 0 || SND_LIB_MINOR >= 6
#define snd_seq_addr_equal(a,b)	((a)->client == (b)->client && (a)->port == (b)->port)
    case SND_SEQ_EVENT_PORT_SUBSCRIBED:
        if (snd_seq_addr_equal(&aevp->data.connect.dest, &aevp->dest)) {
            if (! ctxp->active) {
                if (! start_sequencer(ctxp)) {
                    snd_seq_free_event(aevp);
                    return 0;
                }
            }
            BASS_MIDI_StreamEvent( hStream[aevp->dest.port], 0, MIDI_EVENT_SYSTEM, MIDI_SYSTEM_DEFAULT );
            ctxp->used++;
        }
        break;

    case SND_SEQ_EVENT_PORT_UNSUBSCRIBED:
        if (snd_seq_addr_equal(&aevp->data.connect.dest, &aevp->dest)) {
            if (ctxp->active) {
                ctxp->used--;
                if (ctxp->used <= 0) {
                    snd_seq_free_event(aevp);
                    return 1; /* quit now */
                }
            }
        }
        break;
#else
    case SND_SEQ_EVENT_PORT_USED:
        if (! ctxp->active) {
            if (! start_sequencer(ctxp)) {
                snd_seq_free_event(aevp);
                return 0;
            }
        }
        BASS_MIDI_StreamEvent( hStream[aevp->dest.port], 0, MIDI_EVENT_SYSTEM, MIDI_SYSTEM_DEFAULT );
        ctxp->used++;
        break;

    case SND_SEQ_EVENT_PORT_UNUSED:
        if (ctxp->active) {
            ctxp->used--;
            if (ctxp->used <= 0) {
                snd_seq_free_event(aevp);
                return 1; /* quit now */
            }
        }
        break;
#endif

    default:
        break;
    }
    snd_seq_free_event(aevp);
    return 0;
}

#if SND_LIB_MAJOR > 0 || SND_LIB_MINOR >= 6
/* !! this is a dirty hack.  not sure to work in future !! */
int snd_seq_file_descriptor(snd_seq_t *handle)
{
    int pfds = snd_seq_poll_descriptors_count(handle, POLLIN);
    if (pfds > 0) {
        struct pollfd pfd;
        if (snd_seq_poll_descriptors(handle, &pfd, 1, POLLIN) >= 0)
            return pfd.fd;
    }
    return -ENXIO;
}

int alsa_seq_open(snd_seq_t **seqp)
{
    return snd_seq_open(seqp, "hw", SND_SEQ_OPEN_DUPLEX, 0);
}

int alsa_create_port(snd_seq_t *seq, int index)
{
    char name[32];
    int port;

    sprintf(name, "BASSMIDI port %d", index);
    port = snd_seq_create_simple_port(seq, name,
                      SND_SEQ_PORT_CAP_WRITE|SND_SEQ_PORT_CAP_SUBS_WRITE,
                      SND_SEQ_PORT_TYPE_MIDI_GENERIC);
    if (port < 0) {
        fprintf(stderr, "error in snd_seq_create_simple_port\n");
        return -1;
    }

    return port;
}

#else
int alsa_seq_open(snd_seq_t **seqp)
{
    return snd_seq_open(seqp, SND_SEQ_OPEN_IN);
}

int alsa_create_port(snd_seq_t *seq, int index)
{
    snd_seq_port_info_t pinfo;

    memset(&pinfo, 0, sizeof(pinfo));
    sprintf(pinfo.name, "BASSMIDI port %d", index);
    pinfo.capability = SND_SEQ_PORT_CAP_WRITE|SND_SEQ_PORT_CAP_SUBS_WRITE;
    pinfo.type = SND_SEQ_PORT_TYPE_MIDI_GENERIC;
    strcpy(pinfo.group, SND_SEQ_GROUP_DEVICE);
    if (snd_seq_create_port(alsactx.handle, &pinfo) < 0) {
        fprintf(stderr, "error in snd_seq_create_simple_port\n");
        return -1;
    }
    return pinfo.port;
}
#endif

void alsa_set_timestamping(struct seq_context *ctxp, int port)
{
#if HAVE_SND_SEQ_PORT_INFO_SET_TIMESTAMPING
    int q = 0;
    snd_seq_port_info_t *pinfo;

    if (ctxp->queue < 0) {
        q = snd_seq_alloc_queue(ctxp->handle);
        ctxp->queue = q;
        if (q < 0)
            return;
        if (snd_seq_queue_status_malloc(&ctxp->q_status) < 0) {
            fprintf(stderr, "no memory!\n");
            exit(1);
        }
    }

    snd_seq_port_info_alloca(&pinfo);
    if (snd_seq_get_port_info(ctxp->handle, port, pinfo) < 0)
        return;
    snd_seq_port_info_set_timestamping(pinfo, 1);
    snd_seq_port_info_set_timestamp_real(pinfo, 1);
    snd_seq_port_info_set_timestamp_queue(pinfo, q);
    if (snd_seq_set_port_info(ctxp->handle, port, pinfo) < 0)
        return;
#endif
}

void sig_timeout(int sig)
{
    signal(SIGALRM, sig_timeout); /* For SysV base */
    /* Expect EINTR */
}

/* reset all when SIGHUP is received */
void sig_reset(int sig)
{
    if (alsactx.active) {
        stop_sequencer(&alsactx);
        server_reset();
    }
    signal(SIGHUP, sig_reset);
}

/* handle termination */
void sig_close(int sig)
{
    is_running = 0;
    signal(sig, sig_close);
}

/*
 * set the process to realtime privs
 */
int set_realtime_priority(void)
{
    struct sched_param schp;
    int max_prio;

    if (opt_realtime_priority <= 0)
        return 0;

    memset(&schp, 0, sizeof(schp));
    max_prio = sched_get_priority_max(SCHED_FIFO);
    if (max_prio < opt_realtime_priority)
        opt_realtime_priority = max_prio;

    schp.sched_priority = opt_realtime_priority;
    if (sched_setscheduler(0, SCHED_FIFO, &schp) != 0) {
        return -1;
    }
    /* drop root priv. */
    if (! geteuid() && getuid() != geteuid()) {
        setuid(getuid());
    }
    return 0;
}
